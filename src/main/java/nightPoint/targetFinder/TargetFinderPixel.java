package nightPoint.targetFinder;

import nightPoint.util.ColorChecker;
import nightPoint.util.Coordinate;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Set;

public class TargetFinderPixel implements TargetFinder {

    private final Coordinate unitSize = new Coordinate(25, 50);

    @Override
    public Coordinate getTarget(BufferedImage searchSpace, Set<ColorChecker> colorCheckers) {

        for (int y = 0; y < searchSpace.getHeight(); y++) {
            for (int x = 0; x < searchSpace.getWidth(); x++) {
                int pixel = searchSpace.getRGB(x, y);
                boolean foundRed = checkPixel(pixel, colorCheckers);

                if (foundRed) {
                    return Coordinate.add(new Coordinate(x, y), unitSize);
                }
            }
        }

        return null;
    }

    private boolean checkPixel(int pixel, Set<ColorChecker> colorCheckers) {
        Color color = new Color(pixel);
        return colorCheckers
                .stream()
                .anyMatch(colorChecker -> colorChecker.check(color));
    }
}
