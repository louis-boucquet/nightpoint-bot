package nightPoint.targetFinder;

import nightPoint.util.ColorChecker;
import nightPoint.util.Coordinate;

import java.awt.image.BufferedImage;
import java.util.Set;

public interface TargetFinder {

    /**
     * Get a target from the search space
     *
     * @param searchSpace an image which might contain a target
     * @param colorCheckers the colors you want to look for
     *
     * @return the coordinate of a target or null if there's no target found
     */
    Coordinate getTarget(BufferedImage searchSpace, Set<ColorChecker> colorCheckers);
}
