package nightPoint.targetFinder;

public class TargetFinders {
    public static final TargetFinder BY_PIXEL = new TargetFinderPixel();
    public static final TargetFinder BY_BAR = new TargetFinderBar();

    private TargetFinders() { }
}
