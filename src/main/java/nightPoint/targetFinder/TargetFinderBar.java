package nightPoint.targetFinder;

import nightPoint.util.ColorChecker;
import nightPoint.util.Coordinate;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Set;

public class TargetFinderBar implements TargetFinder {

    private final Coordinate unitSize = new Coordinate(25, 50);

    @Override
    public Coordinate getTarget(BufferedImage searchSpace, Set<ColorChecker> colorCheckers) {
        Coordinate startOfBar = null;

        for (int y = 0; y < searchSpace.getHeight(); y++) {
            for (int x = 0; x < searchSpace.getWidth(); x++) {
                int pixel = searchSpace.getRGB(x, y);
                boolean foundRed = checkPixel(pixel, colorCheckers);

                if (startOfBar == null) {
                    if (foundRed) {
                        startOfBar = new Coordinate(x, y);
                    }
                } else {
                    // check range of bar
                    if (x - startOfBar.x < 10) {
                        if (!foundRed) {
                            startOfBar = null;
                        }
                    } else {
                        return Coordinate.add(startOfBar, unitSize);
                    }
                }
            }
        }

        return null;
    }

    private boolean checkPixel(int pixel, Set<ColorChecker> colorCheckers) {
        Color color = new Color(pixel);
        return colorCheckers
                .stream()
                .anyMatch(colorChecker -> colorChecker.check(color));
    }
}
