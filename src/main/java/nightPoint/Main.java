package nightPoint;

import nightPoint.predictor.Predictors;
import nightPoint.targetFinder.TargetFinders;
import nightPoint.util.ColorChecker;
import nightPoint.util.Coordinate;

import java.util.Arrays;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        new NightPointBot(
                Predictors.LINEAR_WITH_SPEED,
                TargetFinders.BY_PIXEL,
                new Coordinate(250, 250),
                new HashSet<>(Arrays.asList(
                        ColorChecker.BLUE
//                        ColorChecker.RED
//                        ColorChecker.GREEN
                ))
        ).run();
    }
}
