package nightPoint;

import nightPoint.predictor.Predictor;
import nightPoint.predictor.Predictors;
import nightPoint.targetFinder.TargetFinder;
import nightPoint.targetFinder.TargetFinders;
import nightPoint.util.ColorChecker;
import nightPoint.util.Coordinate;
import nightPoint.util.CoordinateTransformer;
import nightPoint.util.Timer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NightPointBot implements Runnable {

    private Robot robot;

    private final Timer timer = new Timer();

    private final Shooter shooter;
    private final Predictor predictor;
    private final TargetFinder targetFinder;
    private final SearchSpaceGetter searchSpaceGetter;
    private final Set<ColorChecker> colorCheckers;

    public NightPointBot(Predictor predictor, TargetFinder targetFinder, Coordinate searchSpaceSize, Set<ColorChecker> colorCheckers) {
        this.colorCheckers = colorCheckers;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        this.predictor = predictor;
        this.targetFinder = targetFinder;

        this.searchSpaceGetter = new SearchSpaceGetter(robot, searchSpaceSize);
        this.shooter = new Shooter(robot, predictor);
    }

    @Override
    public void run() {
        // create and start Shooter Thread
        Thread shooterThread = new Thread(shooter);
        shooterThread.start();

        timer.startTimer();

        while (true) {
            // get mouseLocation
            Coordinate mouseLocation = Coordinate.from(MouseInfo.getPointerInfo().getLocation());

            Coordinate searchSpaceOffset = Coordinate.sub(mouseLocation, searchSpaceGetter.getOffset());

            // get searchSpace
            BufferedImage searchSpace = searchSpaceGetter.getSearchSpace(mouseLocation);

            // get Target
            Coordinate targetFromSearchSpace = targetFinder.getTarget(searchSpace, colorCheckers);

            Coordinate targetFromUnit = null;

            // compensate for mouse location
            if (targetFromSearchSpace != null) {
                Coordinate targetScreen = Coordinate.add(targetFromSearchSpace, searchSpaceOffset);

                targetFromUnit = CoordinateTransformer.toRelative(targetScreen, mouseLocation);

                System.out.println(targetScreen + " -> " + targetFromUnit);
            }

            // update target location
            predictor.newCoordinate(targetFromUnit);

            System.out.print("\r");
            System.out.print(timer.lap());
        }
    }
}
