package nightPoint.predictor;

import nightPoint.util.Coordinate;

public class PredictorNone implements Predictor {
    private Coordinate lastCoordinate;

    @Override
    public void newCoordinate(Coordinate coordinate) {
        lastCoordinate = coordinate;
    }

    @Override
    public Coordinate predict(double dist) {
        return lastCoordinate;
    }
}
