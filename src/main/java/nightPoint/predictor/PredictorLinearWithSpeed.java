package nightPoint.predictor;

import nightPoint.util.Coordinate;

/**
 * This is just a linear predictor based on the last two coordinate
 */
public class PredictorLinearWithSpeed implements Predictor {

    private Coordinate beforeLastCoordinate = null;
    private Coordinate lastCoordinate = null;

    @Override
    public synchronized void newCoordinate(Coordinate coordinate) {
        // shift coordinates
        beforeLastCoordinate = lastCoordinate;

        lastCoordinate = coordinate;
    }

    @Override
    public synchronized Coordinate predict(double dist) {
        if (lastCoordinate != null && beforeLastCoordinate != null)
            return getPrediction(dist);
        else
            return null;
    }

    /**
     * get the prediction after data check
     * @return prediction
     */
    private Coordinate getPrediction(double dist) {
        // calculate last known speed
        Coordinate speed = Coordinate.sub(lastCoordinate, beforeLastCoordinate);
        System.out.println("speed " + speed);
        double fac = 1 + dist * 0.0012;
        speed = Coordinate.multiply(speed, fac);

        // add speed to the current location
        return Coordinate.add(lastCoordinate, speed);
    }
}
