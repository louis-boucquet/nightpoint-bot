package nightPoint.predictor;

/**
 * This will contain all implemented predictors
 */
public class Predictors {
    public static Predictor LINEAR = new PredictorLinear();
    public static Predictor LINEAR_WITH_SPEED = new PredictorLinearWithSpeed();
    public static Predictor NONE = new PredictorNone();

    private Predictors() { }
}
