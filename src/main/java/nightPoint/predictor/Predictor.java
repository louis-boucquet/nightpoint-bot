package nightPoint.predictor;

import nightPoint.util.Coordinate;

/**
 * This predicts the next coordinate based on previous
 */
public interface Predictor {

    /**
     * add a new data point
     * @param coordinate the new coordinate
     */
    void newCoordinate(Coordinate coordinate);

    /**
     * get the prediction of the next Coordinate
     * @return the prediction
     */
    Coordinate predict(double dist);
}
