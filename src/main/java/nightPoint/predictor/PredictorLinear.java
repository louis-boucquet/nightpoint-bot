package nightPoint.predictor;

import nightPoint.util.Coordinate;

/**
 * This is just a linear predictor based on the last two coordinate
 */
public class PredictorLinear implements Predictor {

    private Coordinate beforeLastCoordinate = null;
    private Coordinate lastCoordinate = null;

    @Override
    public void newCoordinate(Coordinate coordinate) {
        // shift coordinates
        beforeLastCoordinate = lastCoordinate;

        lastCoordinate = coordinate;
    }

    @Override
    public Coordinate predict(double dist) {
        if (lastCoordinate != null && beforeLastCoordinate != null)
            return getPrediction();
        else
            return null;
    }

    /**
     * get the prediction after data check
     * @return prediction
     */
    private Coordinate getPrediction() {
        // calculate last known speed
        Coordinate speed = Coordinate.sub(lastCoordinate, beforeLastCoordinate);

        // add speed to the current location
        return Coordinate.add(lastCoordinate, speed);
    }
}
