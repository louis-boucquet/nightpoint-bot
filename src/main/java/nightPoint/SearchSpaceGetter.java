package nightPoint;

import nightPoint.util.Coordinate;

import java.awt.*;
import java.awt.image.BufferedImage;

public class SearchSpaceGetter {

    private final Robot robot;
    private final Coordinate size;
    private final Coordinate offset;

    public SearchSpaceGetter(Robot robot, Coordinate size) {
        this.robot = robot;
        this.size = size;
        this.offset = Coordinate.multiply(size, 0.5);
    }

    public BufferedImage getSearchSpace(Coordinate middle) {
        Rectangle screenRect = new Rectangle(
                middle.x - offset.x,
                middle.y - offset.y,
                size.x, size.y);

        return robot.createScreenCapture(screenRect);
    }

    public Coordinate getOffset() {
        return offset;
    }
}
