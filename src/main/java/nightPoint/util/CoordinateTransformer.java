package nightPoint.util;

/**
 * Some methods to trans Coordinates
 */
public class CoordinateTransformer {
    private static Coordinate screenSize = new Coordinate(1920, 1080);
    private static Coordinate screenCenter = Coordinate.multiply(screenSize, 0.5);

    /**
     * convert from relative coordinates to screen coordinates
     * @param relativeCoordinate coordinate to convert
     * @param mouseCoordinate mouse location
     * @return converted coordinate
     */
    public static Coordinate toScreen(Coordinate relativeCoordinate, Coordinate mouseCoordinate) {
        // get unit location
        Coordinate unitLocation = getUnitLocation(mouseCoordinate);

        return Coordinate.add(unitLocation, relativeCoordinate);
    }

    /**
     * convert from screen coordinates to relative coordinates
     * @param screenCoordinate coordinate to convert
     * @param mouseCoordinate mouse location
     * @return converted coordinate
     */
    public static Coordinate toRelative(Coordinate screenCoordinate, Coordinate mouseCoordinate) {
        // get unit location
        Coordinate unitLocation = getUnitLocation(mouseCoordinate);

        return Coordinate.sub(screenCoordinate, unitLocation);
    }

    /**
     * @param mouseLocation current location of the mouse
     * @return unit's distance form the center
     */
    private static Coordinate getUnitLocation(Coordinate mouseLocation) {
        Coordinate mouseDiffFromCenter = Coordinate.sub(mouseLocation, screenCenter);
        Coordinate unitDiffFromCenter = new Coordinate((int) (mouseDiffFromCenter.x * 1.0 / 3.0), (int) (mouseDiffFromCenter.y * 0.4));

        return Coordinate.add(screenCenter, unitDiffFromCenter);
    }

    /**
     * Don't allow instances
     */
    private CoordinateTransformer() { }
}
