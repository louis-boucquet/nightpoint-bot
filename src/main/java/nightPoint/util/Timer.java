package nightPoint.util;

public class Timer {

    private long current;

    public void startTimer() {
        current = System.currentTimeMillis();
    }

    public long lap() {
        long lastMillis = current;
        current = System.currentTimeMillis();

        return current - lastMillis;
    }
}
