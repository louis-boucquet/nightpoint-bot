package nightPoint.util;

import java.awt.*;

public interface ColorChecker {
    ColorChecker RED = c -> c.getRed() > 130 && c.getGreen() < 20 && c.getBlue() < 20;
    ColorChecker GREEN = c -> c.getRed() < 20 && c.getGreen() > 130 && c.getBlue() < 20;
    ColorChecker BLUE = c -> c.getRed() < 20 && c.getGreen() < 20 && c.getBlue() > 130;

    boolean check(Color c);
}
