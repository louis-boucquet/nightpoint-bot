package nightPoint.util;

import java.awt.*;

public class Coordinate {
    public final int x;
    public final int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Coordinate sub(Coordinate c1, Coordinate c2) {
        return new Coordinate(c1.x - c2.x, c1.y - c2.y);
    }

    public static Coordinate add(Coordinate c1, Coordinate c2) {
        return new Coordinate(c1.x + c2.x, c1.y + c2.y);
    }

    public static double dist(Coordinate c1, Coordinate c2) {
        return Math.sqrt(Math.pow(c1.x - c2.x, 2) + Math.pow(c1.y - c2.y, 2));
    }

    public static Coordinate from(Point target) {
        if (target == null)
            return null;
        return new Coordinate(target.x, target.y);
    }

    public static Coordinate multiply(Coordinate size, double factor) {
        return new Coordinate((int) (size.x * factor), (int) (size.y * factor));
    }

    @Override
    public String toString() {
        return "("  + x + ", " + y + ')';
    }
}
