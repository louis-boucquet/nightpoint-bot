package nightPoint;

import nightPoint.predictor.Predictor;
import nightPoint.util.Coordinate;
import nightPoint.util.CoordinateTransformer;

import java.awt.*;
import java.awt.event.InputEvent;

public class Shooter implements Runnable {

    private final Robot robot;
    private final Predictor predictor;

    public Shooter(Robot robot, Predictor predictor) {
        this.robot = robot;
        this.predictor = predictor;
    }

    @Override
    public void run() {
        while (true) {
            // get mouseLocation
            Coordinate mouseLocation = Coordinate.from(MouseInfo.getPointerInfo().getLocation());

            // get distance from center of screen
            double dist = Coordinate.dist(new Coordinate(960, 540), mouseLocation);

            // get target prediction
            Coordinate target = predictor.predict(dist);

            // if there's a target, shoot it
            if (target != null) {
                Coordinate targetScreen = CoordinateTransformer.toScreen(target, mouseLocation);

                shoot(targetScreen);
            } else {
                sleep(20);
            }
        }
    }

    private void shoot(Coordinate target) {

        robot.mouseMove(target.x, target.y);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        sleep(10);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
//        robot.mouseMove(mouseLocation.x, mouseLocation.y);
        sleep(10);
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
